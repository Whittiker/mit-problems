# Problem Set 4A
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

def get_permutations(sequence):
    '''
    Enumerate all permutations of a given string

    sequence (string): an arbitrary string to permute.

    Assume that it is a non-empty string.

    You MUST use recursion for this part. Non-recursive solutions will not be
    accepted.

    Returns: a list of all permutations of sequence

    Example:
    >>> get_permutations('abc')
    ['abc', 'acb', 'bac', 'bca', 'cab', 'cba']

    Note: depending on your implementation, you may return the permutations in
    a different order than what is listed here.
    '''
    #### only this one but note the below function was not written by me!   #need to come back and do this
    def Permute(string):
        if len(string) == 0:
            return ['']
        prevList = Permute(string[1:len(string)])
        nextList = []
        for i in range(0, len(prevList)):
            for j in range(0, len(string)):
                newString = prevList[i][0:j] + string[0] + prevList[i][j:len(string) - 1]
                if newString not in nextList:
                    nextList.append(newString)
        return nextList
    return Permute(sequence)


if __name__ == '__main__':
#    #EXAMPLE
#    example_input = 'abc'
#    print('Input:', example_input)
#    print('Expected Output:', ['abc', 'acb', 'bac', 'bca', 'cab', 'cba'])
#    print('Actual Output:', get_permutations(example_input))
    
#    # Put three example test cases here (for your sanity, limit your inputs
#    to be three characters or fewer as you will have n! permutations for a 
#    sequence of length n)
    print("--------- TEST 1 ---------")
    print('Input: si')
    print('Expected Output:', ['si', 'is'])
    print('Actual Output:', get_permutations("si"))
    print("\n", "--------- TEST 2 ---------")
    print('Input: s')
    print('Expected Output:', ['s'])
    print('Actual Output:', get_permutations("s"))
    print("\n", "--------- TEST 3 ---------")
    print('Input: sim')
    print('Expected Output:', ['sim', 'smi', 'iam', 'ims', 'msi', 'mis'])
    print('Actual Output:', get_permutations("sim"))


