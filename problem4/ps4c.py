# Problem Set 4C
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

import string, copy
#from ps4a import get_permuta
from ps4a import get_permutations

### HELPER CODE ###
def load_words(file_name):
    '''
    file_name (string): the name of the file containing 
    the list of words to load    
    
    Returns: a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    '''
    
    print("Loading word list from file...")
    # inFile: file
    inFile = open(file_name, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.extend([word.lower() for word in line.split(' ')])
    print("  ", len(wordlist), "words loaded.")
    return wordlist

def is_word(word_list, word):
    '''
    Determines if word is a valid word, ignoring
    capitalization and punctuation

    word_list (list): list of words in the dictionary.
    word (string): a possible word.
    
    Returns: True if word is in word_list, False otherwise

    Example:
    >>> is_word(word_list, 'bat') returns
    True
    >>> is_word(word_list, 'asdf') returns
    False
    '''
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"")
    return word in word_list


### END HELPER CODE ###

WORDLIST_FILENAME = 'words.txt'

# you may find these constants helpful
VOWELS_LOWER = 'aeiou'
VOWELS_UPPER = 'AEIOU'
CONSONANTS_LOWER = 'bcdfghjklmnpqrstvwxyz'
CONSONANTS_UPPER = 'BCDFGHJKLMNPQRSTVWXYZ'

class SubMessage(object):
    def __init__(self, text):
        '''
        Initializes a SubMessage object
                
        text (string): the message's text

        A SubMessage object has two attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words)
        '''
        self.message_text = text
        self.valid_words = load_words("words.txt")

    @property
    def get_message_text(self):
        '''
        Used to safely access self.message_text outside of the class
        
        Returns: self.message_text
        '''
        return self.message_text

    @property
    def get_valid_words(self):
        '''
        Used to safely access a copy of self.valid_words outside of the class.
        This helps you avoid accidentally mutating class attributes.
        
        Returns: a COPY of self.valid_words
        '''
        return copy.deepcopy(self.valid_words)
                
    def build_transpose_dict(self, vowels_permutation):
        '''
        vowels_permutation (string): a string containing a permutation of vowels (a, e, i, o, u)
        
        Creates a dictionary that can be used to apply a cipher to a letter.
        The dictionary maps every uppercase and lowercase letter to an
        uppercase and lowercase letter, respectively. Vowels are shuffled 
        according to vowels_permutation. The first letter in vowels_permutation 
        corresponds to a, the second to e, and so on in the order a, e, i, o, u.
        The consonants remain the same. The dictionary should have 52 
        keys of all the uppercase letters and all the lowercase letters.

        Example: When input "eaiuo":
        Mapping is a->e, e->a, i->i, o->u, u->o
        and "Hello World!" maps to "Hallu Wurld!"

        Returns: a dictionary mapping a letter (string) to 
                 another letter (string). 
        '''
        letters = string.ascii_lowercase + string.ascii_uppercase
        index = 0
        index_upper = 0
        transpose_dict = {}
        for l in letters:
            if l in VOWELS_LOWER:
                transpose_dict["{}".format(l)] = vowels_permutation[index]
                index += 1                                      #so we can move threw vowels_permutation str
            elif l in VOWELS_UPPER:
                transpose_dict["{}".format(l)] = vowels_permutation[index_upper]
                index_upper += 1
            else:
                transpose_dict["{}".format(l)] = l
        return transpose_dict



    def apply_transpose(self, transpose_dict):
        '''
        transpose_dict (dict): a tranf the message text, based
        on the dictionary
        '''
        transpose = []
        letters = string.ascii_lowercase + string.ascii_uppercase
        copys = copy.deepcopy(self.message_text)
        for l in copys:
            if l in letters:
                transpose.append(transpose_dict["{}".format(l)])    # add to list values from tran dic
            if l in " !@#$%^&*()-_+={}[]|\:;'<>?,./\"":
                transpose.append(l)                                 # keeps spaces
        joined = "".join(transpose)     # converts list of leters to string
        return joined


class EncryptedSubMessage(SubMessage):
    def __init__(self, text):
        SubMessage.__init__(self, text)
        '''
        Initializes an EncryptedSubMessage object

        text (string): the encrypted message text

        An EncryptedSubMessage object inherits from SubMessage and has two attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words)
        '''


    def decrypt_message(self):
        '''
        Attempt to decrypt the encrypted message 
        
        Idea is to go through each permutation of the vowels and test it
        on the encrypted message. For each permutation, check how many
        words in the decrypted text are valid English words, and return
        the decrypted message with the most English words.
        
        If no good permutations are found (i.e. no permutations result in 
        at least 1 valid word), return the original string.
        If there are
        multiple permutations that yield the maximum number of words, retur, 'bac', 'bca', 'cab', 'cba']n any
        one of them.

        Returns: the best decrypted message    
        
        Hint: use your function from Part 4A
        '''
        perm_list = get_permutations("aeiou")
        results_dic = {}
        valid_words = load_words("words.txt")
        for perm in perm_list:
            tran_dic = SubMessage.build_transpose_dict(self, perm)    # create new dictionary for perm of aeiou
            transformed = SubMessage.apply_transpose(self, tran_dic)  # apply tran
            transformed_list = transformed.split(" ")                 # create list of words from string
            valid_count = 0
            for word in transformed_list:
                if is_word(valid_words, word):                # word is valid then add one to the count
                    valid_count += 1
            results_dic["{}".format(perm)] = valid_count


        large_val = sorted(results_dic.values(), reverse=True)[0]     # using permutation as key count number of  valid words
        for key, value in results_dic.items():
            if value == large_val:
                if value > 0:                                         # checks if we have any valid words
                    best_perm = key
                else:

                    return self.message_text

        final_dic = SubMessage.build_transpose_dict(self, best_perm)  # create new dictionary for our answer perm
        answer = SubMessage.apply_transpose(self, final_dic)          # performs transform with best perm
        return answer



if __name__ == '__main__':

    # Example test case
    message = SubMessage("Hello World!")
    permutation = "eaiuo"
    enc_dict = message.build_transpose_dict(permutation)
    print("Original message:", message.get_message_text, "Permutation:", permutation)
    print("Expected encryption:", "Hallu Wurld!")
    print("Actual encryption:", message.apply_transpose(enc_dict))
    enc_message = EncryptedSubMessage(message.apply_transpose(enc_dict))
    print("Decrypted message:", enc_message.decrypt_message())

    print("\n", "------- TEST CASE 2 -------")
    message = SubMessage("Does this one work Simon!")
    permutation = "uoeai"
    enc_dict = message.build_transpose_dict(permutation)
    print("Original message:", message.get_message_text, "Permutation:", permutation)
    print("Expected encryption:", "Daos thes ano wark Seman?!")
    print("Actual encryption:", message.apply_transpose(enc_dict))
    enc_message = EncryptedSubMessage(message.apply_transpose(enc_dict))
    print("Decrypted message:", enc_message.decrypt_message())

    print("\n", "------- TEST CASE 3 -------")
    message = SubMessage("rndum missplt wrds")
    permutation = "ieaou"
    enc_dict = message.build_transpose_dict(permutation)
    print("Original message:", message.get_message_text, "Permutation:", permutation)
    print("Expected encryption:", "rndum massplt wrds")
    print("Actual encryption:", message.apply_transpose(enc_dict))
    enc_message = EncryptedSubMessage(message.apply_transpose(enc_dict))
    print("Decrypted message:", enc_message.decrypt_message())