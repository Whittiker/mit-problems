# Problem Set 4B
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx


### HELPER CODE ###

import string, copy

def load_words(file_name):
    '''
    file_name (string): the name of the file containing
    the list of words to load

    Returns: a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    '''
    print("Loading word list from file...")
    # inFile: file
    inFile = open(file_name, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.extend([word.lower() for word in line.split(' ')])
    print("  ", len(wordlist), "words loaded.")
    return wordlist

def is_word(word_list, word):
    '''
    Determines if word is a valid word, ignoring
    capitalization and punctuation

    word_list (list): list of words in the dictionary.
    word (string): a possible word.

    Returns: True if word is in word_list, False otherwise

    Example:
    >>> is_word(word_list, 'bat') returns
    True
    >>> is_word(word_list, 'asdf') returns
    False
    '''
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"")
    return word in word_list

def get_story_string():
    """
    Returns: a story in encrypted text.
    """
    f = open("story.txt", "r")
    story = str(f.read())
    f.close()
    return story

### END HELPER CODE ###

WORDLIST_FILENAME = 'words.txt'

class Message(object):
    def __init__(self, text, file_name):    #text is the message
        self.message_text = text                 # put here by new
        self.valid_words = load_words(file_name)
        '''
       Initializes a Message object

       text (string): the message's text

       a Message object has two attributes:
           self.message_text (string, determined by input text)
           self.valid_words (list, determined using helper function load_words)
       '''

    @property                     # GETTER
    def get_message_text(self):
        '''
        Used to safely access self.message_text outside of the class

        Returns: self.message_text
        '''
        return self.message_text

    @property                     # GETTER
    def get_valid_words(self):
        '''
        Used to safely access a copy of self.valid_words outside of the class.
        This helps you avoid accidentally mutating class attributes.

        Returns: a COPY of self.valid_words
        '''
        copy_valid_words = self.valid_words[:]    #creates a copy of the list
        return copy_valid_words

    def build_shift_dict(self, shift):   # why need self here
        '''
        Creates a dictionary that can be used to apply a cipher to a letter.
        The dictionary maps every uppercase and lowercase letter to a
        character shifted down the alphabet by the input shift.
        The dictionary
        should have 52 keys of all the uppercase letters and all the lowercase
        letters only.

        shift (integer): the amount by which to shift every letter of the
        alphabet. 0 <= shift < 26

        Returns: a dictionary mapping a letter (string) to
                 another letter (string).
        '''
        cipher_shift = {}
        for i in range(len(string.ascii_lowercase)):
            shift_mod26 = (i + shift) % 26                                                # so we stay in index
            cipher_shift[string.ascii_lowercase[i]] = string.ascii_lowercase[shift_mod26] #keys alpha values alpha shifted reduced mod 26
        for i in range(len(string.ascii_uppercase)):
            shift_mod26 = (i + shift) % 26
            cipher_shift[string.ascii_uppercase[i]] = string.ascii_uppercase[shift_mod26]

        return cipher_shift


    def apply_shift(self, shift):
        '''
        Applies the Caesar Cipher to self.message_text with the input shift.
        Creates a new string that is self.message_text shifted down the
        alphabet by some number of characters determined by the input shift

        shift (integer): the shift with which to encrypt the message.
        0 <= shift < 26

        Returns: the message text (string) in which every character is shifted
             down the alphabet by the input shift
        '''

        cipher_dic = self.build_shift_dict(shift)
        new_str = []
        letters = string.ascii_lowercase + string.ascii_uppercase
        LT_letters = list(letters)
        for i in self.message_text:                                # goes threw message letter by letter
            if i in LT_letters:                                    # so we don't try and change " " or punc
                new_str.append(cipher_dic[i])                      # uses dic to find the shifter letter
            else:
                new_str.append(i)
        message_text = "".join(new_str)
        return message_text


class PlaintextMessage(Message):
    def __init__(self, text, file_name, shift):
        Message.__init__(self, text, file_name)
        '''
        Initializes a PlaintextMessage object        
        
        text (string): the message's text
        shift (integer): the shift associated with this message

        A PlaintextMessage object inherits from Message and has five attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words)
            self.shift (integer, determined by input shift)
            self.encryption_dict (dictionary, built using shift)
            self.message_text_encrypted (string, created using shift)

        '''

        self.shift = shift
        self.encryption_dict = Message.build_shift_dict(self, shift)
        self.message_text_encrypted = Message.apply_shift(self, shift)

    @property
    def get_shift(self):
        '''
        Used to safely access self.shift outside of the class

        Returns: self.shift
        '''

        return self.shift

    @property
    def get_encryption_dict(self):
        '''
        Used to safely access a copy self.encryption_dict outside of the class

        Returns: a COPY of self.encryption_dict
        '''
        #using the copy module
        copy_encryption_dict = copy.deepcopy(self.encryption_dict)
        return copy_encryption_dict


    @property
    def get_message_text_encrypted(self):
        '''
        Used to safely access self.message_text_encrypted outside of the class

        Returns: self.message_text_encrypted
        '''
        return self.message_text_encrypted

    @get_shift.setter         #this references the function which gets shift
    def change_shift(self, shift):  #input setter here FOOTBALL
        '''
        Changes self.shift of the PlaintextMessage and updates other
        attributes determined by shift.

        shift (integer): the new shift that should be associated with this message.
        0 <= shift < 26

        Returns: nothing
        '''
        self.shift = shift    #HOW DO I UPDATE other attributes determined by shift
        self.encryption_dict = build_shift_dict(self, shift)
        self.message_text_encrypted = apply_shift(self, shift)

class CiphertextMessage(Message):
    def __init__(self, text, file_name):
        Message.__init__(self, text, file_name)
        '''
        Initializes a CiphertextMessage object
                
        text (string): the message's text

        a CiphertextMessage object has two attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words)
        '''

    def decrypt_message(self):
        '''
        Decrypt self.message_text by trying every possible shift value
        and find the "best" one. We will define "best" as the shift that
        creates the maximum number of real words when we use apply_shift(shift)
        on the message text. If s is the original shift value used to encrypt
        the message, then we would expect 26 - s to be the best shift value
        for decrypting it.

        Note: if multiple shifts are equally good such that they all create
        the maximum number of valid words, you may choose any of those shifts
        (and their corresponding decrypted messages) to return

        Returns: a tuple of the best shift value used to decrypt the message
        and the decrypted message text using that shift value
        '''

        shift_tally = {}
        for i in range(26):
            decoded = self.apply_shift(26 - i)           # applys shift the other way
            decodedLT = decoded.split(" ")                # splits string by spaces
            tally = 0                                      # resets tally for each shift tired
            for word_ in decodedLT:
                if is_word(self.valid_words, word_) == True:
                    tally += 1
            shift_tally["{}".format(i)] = tally


        answer = sorted(shift_tally.values(), reverse = True)[0]   # gives max value
        for key in shift_tally.keys():                             # attempting to find the key that matches highest value
            if shift_tally[key] == answer:
                best_match = self.apply_shift((26 - int(key)))
                best_match.split(" ")
                best_match_str = "".join(best_match)
                return ((26 - int(key)), best_match_str)


if __name__ == "__main__":
    # Test case PlaintextMessage
    plaintext = PlaintextMessage('hello', "words.txt", 2)
    print("------------  Test 1 PlainTextMessage  ------------")
    print('Expected Output: jgnnq')
    print('Actual Output:', plaintext.get_message_text_encrypted)
    print("------------------------")

    plaintext2 = PlaintextMessage('simon testy boi!', "words.txt", 3)
    print("------------  Test 2 PlainTextMessage  ------------")
    print('Expected Output: vlprq whvwb erl!')
    print('Actual Output:', plaintext2.get_message_text_encrypted)
    print("------------------------")


    #Test case CiphertextMessage
    ciphertext = CiphertextMessage('jgnnq', 'words.txt')
    print("------------  Test 1 CiphertextMessage  ------------")
    print('Expected Output:', (24, 'hello'))
    print('Actual Output:', ciphertext.decrypt_message())
    print("------------------------")

    ciphertext2 = CiphertextMessage('vlprq whvwb erl', 'words.txt')
    print("------------  Test 2 CiphertextMessage  ------------")
    print('Expected Output:', (23, 'simon testy boi'))
    print('Actual Output:', ciphertext2.decrypt_message())
    print("------------------------")

    story = " ".join(load_words("story.txt"))
    cipher = CiphertextMessage(story, 'words.txt')
    print('Actual Output:', cipher.decrypt_message())

    #TODO: best shift value and unencrypted story
