def Permute(string):
    if len(string) == 0:
        return ['']
    prevList = Permute(string[1:len(string)])
    nextList = []
    for i in range(len(prevList)):
        for j in range(len(string)):
            newString = prevList[i][0:j] + string[0] + prevList[i][j:len(string) - 1]
            if newString not in nextList:
                nextList.append(newString)
    return nextList

print(Permute("abc"))