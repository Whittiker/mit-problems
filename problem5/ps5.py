# 6.0001/6.00 Problem Set 5 - RSS Feed Filter
# Name:
# Collaborators:
# Time:

import feedparser
import string
import time
import threading
from project_util import translate_html
from mtTkinter import *
from datetime import datetime
import pytz
import copy

utc=pytz.UTC

#-----------------------------------------------------------------------

#======================
# Code for retrieving and parsing
# Google and Yahoo News feeds
# Do not change this code
#======================

def process(url):
    """
    Fetches news items from the rss url and parses them.
    Returns a list of NewsStory-s.
    """
    feed = feedparser.parse(url)
    entries = feed.entries
    ret = []
    for entry in entries:
        guid = entry.guid
        title = translate_html(entry.title)
        link = entry.link
        description = translate_html(entry.description)
        pubdate = translate_html(entry.published)

        try:
            pubdate = datetime.strptime(pubdate, "%a, %d %b %Y %H:%M:%S %Z")
            pubdate.replace(tzinfo=pytz.timezone("GMT"))
          #  pubdate = pubdate.astimezone(pytz.timezone('EST'))
          #  pubdate.replace(tzinfo=None)
        except ValueError:
            pubdate = datetime.strptime(pubdate, "%a, %d %b %Y %H:%M:%S %z")

        newsStory = NewsStory(guid, title, description, link, pubdate)
        ret.append(newsStory)
    return ret

#======================
# Data structure design
#======================

# Problem 1

class NewsStory(object):
    def __init__(self, guid, title, description, link, pubdate):
        self.guid = guid
        self.title = title
        self.description = description
        self.link = link
        self.pubdate = pubdate

    def get_guid(self):
        return self.guid

    def get_title(self):
        return self.title

    def get_description(self):
        return self.description

    def get_link(self):
        return self.link

    def get_pubdate(self):
        return self.pubdate

#======================
# Triggers
#======================

class Trigger(object):
    def evaluate(self):
        """
        Returns True if an alert should be generated
        for the given news item, or False otherwise.
        """
        raise NotImplementedError
        
class PhraseTrigger(Trigger):
    def __init__(self, phrase):
        self.phrase = phrase

    def is_phrase_in(self, String):
        l_phrase = self.phrase.lower()
        String = String.lower()
        letters = string.ascii_letters
        punc = string.punctuation

        for i in punc:
            String = String.replace(i, " ")

        String = " ".join(String.split())             # add a space between each word
        
        if l_phrase in String:
            for letter in letters:
                if (l_phrase + letter or letter + l_phrase) in String:    #tests for case hi in his or hi in thi
                    return False
            return True

            
class TitleTrigger(PhraseTrigger):
    def __init__(self, title):
        PhraseTrigger.__init__(self, title)

    def evaluate(self, news_story):
        Title = news_story.get_title()
        return self.is_phrase_in(Title)

class DescriptionTrigger(PhraseTrigger):
    def __init__(self, description):
        PhraseTrigger.__init__(self, description)

    def evaluate(self, news_story):
        Description = news_story.get_description()
        return self.is_phrase_in(Description)

class TimeTrigger(Trigger):
    def __init__(self, EST):
        datetime_object = datetime.strptime(EST, "%d %b %Y %H:%M:%S")  
        self.EST = datetime_object.replace(tzinfo=None)

class BeforeTrigger(TimeTrigger):
    def evaluate(self, news_story):
        Time = news_story.get_pubdate().replace(tzinfo=None)
        if self.EST < Time:
            return False
        else:
            return True

class AfterTrigger(TimeTrigger):
    def evaluate(self, news_story):
        Time = news_story.get_pubdate().replace(tzinfo=None)
        if self.EST > Time:
            return False
        else:
            return True

class NotTrigger(Trigger):
    def __init__(self, trigger):
        self.trigger = trigger
        
    def evaluate(self, news_story):
        return not self.trigger.evaluate(news_story)

class AndTrigger(Trigger):
    def __init__(self, trigger1, trigger2):
        self.trigger1 = trigger1
        self.trigger2 = trigger2

    def evaluate(self, news_story):
        return self.trigger1.evaluate(news_story) and self.trigger2.evaluate(news_story)

class OrTrigger(Trigger):
    def __init__(self, trigger1, trigger2):
        self.trigger1 = trigger1
        self.trigger2 = trigger2

    def evaluate(self, news_story):
        return self.trigger1.evaluate(news_story) or self.trigger2.evaluate(news_story)

#======================
# Filtering
#======================

# Problem 10
def filter_stories(stories, triggerlist):
    """
    Takes in a list of NewsStory instances.

    Returns: a list of only the stories for which a trigger in triggerlist fires.
    """
    # TODO: Problem 10
    # This is a placeholder
    # (we're just returning all the stories, with no filtering)
    filtered_stories = []
    for story in stories:
        for trigger in triggerlist:
            if trigger.evaluate(story):
                filtered_stories.append(story)
    return filtered_stories

#### here 

#======================
# User-Specified Triggers
#======================
# Problem 11
def read_trigger_config(filename):
    """
    filename: the name of a trigger configuration file

    Returns: a list of trigger objects specified by the trigger configuration
        file.
    """
    # We give you the code to read in the file and eliminate blank lines and
    # comments. You don't need to know how it works for now!
    trigger_file = open(filename, 'r')
    lines = []
    for line in trigger_file:
        line = line.rstrip()
        if not (len(line) == 0 or line.startswith('//')):
            lines.append(line)

    wordsLT = []         # to make a list of lists rather then divided by ","
    trigger_types = {}   # to hold trigger values
    for line in lines:
        split = line.split(",")
        wordsLT.append(split)

    for i in range(len(wordsLT)):
        if wordsLT[i][1] == "DESCRIPTION":
                    trigger_types[wordsLT[i][0]] = DescriptionTrigger(wordsLT[i][2])
        if wordsLT[i][1] == "TITLE":
                    trigger_types[wordsLT[i][0]] = TitleTrigger(wordsLT[i][2])
        if wordsLT[i][1] == "AFTER":
                    trigger_types[wordsLT[i][0]] = AfterTrigger(wordsLT[i][2])
        if wordsLT[i][1] == "BEFORE":
                    trigger_types[wordsLT[i][0]] = BeforeTrigger(wordsLT[i][2])
        if wordsLT[i][1] == "AND":
                    trigger_types[wordsLT[i][0]] = AndTrigger(wordsLT[i][2], wordsLT[i][3])
        if wordsLT[i][1] == "OR":
                    trigger_types[wordsLT[i][0]] = OrTrigger(trigger_types[wordsLT[i][2]], trigger_types[wordsLT[i][3]])

    output_triggers = []
    for triggers in wordsLT[-1]:
        if triggers == 'ADD':
            pass
        else:
            output_triggers.append(trigger_types[triggers])
    return output_triggers



SLEEPTIME = 120 #seconds -- how often we poll

def main_thread(master):
    # A sample trigger list - you might need to change the phrases to correspond
    # to what is currently in the news
    try:
        t1 = TitleTrigger("election")
        t2 = DescriptionTrigger("Trump")
        t3 = DescriptionTrigger("Clinton")
        t4 = AndTrigger(t2, t3)
        triggerlist = [t1, t4]

        # Problem 11
        # TODO: After implementing read_trigger_config, uncomment this line 
        # triggerlist = read_trigger_config('triggers.txt')
        
        # HELPER CODE - you don't need to understand this!
        # Draws the popup window that displays the filtered stories
        # Retrieves and filters the stories from the RSS feeds
        frame = Frame(master)
        frame.pack(side=BOTTOM)
        scrollbar = Scrollbar(master)
        scrollbar.pack(side=RIGHT,fill=Y)

        t = "Google & Yahoo Top News"
        title = StringVar()
        title.set(t)
        ttl = Label(master, textvariable=title, font=("Helvetica", 18))
        ttl.pack(side=TOP)
        cont = Text(master, font=("Helvetica",14), yscrollcommand=scrollbar.set)
        cont.pack(side=BOTTOM)
        cont.tag_config("title", justify='center')
        button = Button(frame, text="Exit", command=root.destroy)
        button.pack(side=BOTTOM)
        guidShown = []
        def get_cont(newstory):
            if newstory.get_guid() not in guidShown:
                cont.insert(END, newstory.get_title()+"\n", "title")
                cont.insert(END, "\n---------------------------------------------------------------\n", "title")
                cont.insert(END, newstory.get_description())
                cont.insert(END, "\n*********************************************************************\n", "title")
                guidShown.append(newstory.get_guid())

        while True:

            print("Polling . . .", end=' ')
            # Get stories from Google's Top Stories RSS news feed
            stories = process("http://news.google.com/news?output=rss")

            # Get stories from Yahoo's Top Stories RSS news feed
            stories.extend(process("http://news.yahoo.com/rss/topstories"))

            stories = filter_stories(stories, triggerlist)

            list(map(get_cont, stories))
            scrollbar.config(command=cont.yview)


            print("Sleeping...")
            time.sleep(SLEEPTIME)

    except Exception as e:
        print(e)


if __name__ == '__main__':
    root = Tk()
    root.title("Some RSS parser")
    t = threading.Thread(target=main_thread, args=(root,))
    t.start()
    root.mainloop()

