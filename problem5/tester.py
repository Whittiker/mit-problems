
# this file is for personal learning / tinkering 

def read_trigger_config(filename):
    """
    filename: the name of a trigger configuration file

    Returns: a list of trigger objects specified by the trigger configuration
        file.
    """
    # We give you the code to read in the file and eliminate blank lines and
    # comments. You don't need to know how it works for now!
    trigger_file = open(filename, 'r')
    lines = []
    for line in trigger_file:
        line = line.rstrip()
        if not (len(line) == 0 or line.startswith('//')):
            lines.append(line)

    # TODO: Problem 11
    # line is the list of lines that you need to parse and for which you need
    # to build triggers

     # for now, print it so you see what it contains!
    wordsLT = []   # list of lists divided by ,
    triggerLT = []  # list of triggers to go off
    trigger_types = {}
    for line in lines:
        split = line.split(",")
        wordsLT.append(split)

    for i in range(len(wordsLT)):
        if wordsLT[i][0] == "ADD":
            for j in range(1,len(wordsLT[i])):
                triggerLT.append(wordsLT[i][j])

    for i in range(len(wordsLT)):
        # if wordsLT[i][1] in ["DESCRIPTION", "TITLE", "AFTER", "BEFORE"]:
        #     trigger_types[wordsLT[i][0]] = wordsLT[i][1][0] + wordsLT[i][1][1:].lower() + "Trigger"+ "('" + wordsLT[i][2] + "')"
        # if wordsLT[i][1] in ["AND", "OR"]:
        #         trigger_types[wordsLT[i][0]] = wordsLT[i][2] + " " + wordsLT[i][1].lower() + " " + wordsLT[i][3]
        if wordsLT[i][1] == "DESCRIPTION":
                    trigger_types[wordsLT[i][0]] = DescriptionTrigger(wordsLT[i][2])
        if wordsLT[i][1] == "TITLE":
                    trigger_types[wordsLT[i][0]] = TitleTrigger(wordsLT[i][2])
        if wordsLT[i][1] == "AFTER":
                    trigger_types[wordsLT[i][0]] = AfterTrigger(wordsLT[i][2])
        if wordsLT[i][1] == "BEFORE":
                    trigger_types[wordsLT[i][0]] = BeforeTrigger(wordsLT[i][2])
        if wordsLT[i][1] == "AND":
                    trigger_types[wordsLT[i][0]] = AndTrigger(wordsLT[i][2], wordsLT[i][3])
        if wordsLT[i][1] == "OR":
                    trigger_types[wordsLT[i][0]] = OrTrigger(trigger_types[wordsLT[i][2]], trigger_types[wordsLT[i][3]])

    print(wordsLT)
    output_triggers = []
    for triggers in wordsLT[-1]:
        if triggers == 'ADD':
            pass
        else:
            output_triggers.append(trigger_types[triggers])
    return output_triggers
        
    



    print(triggerLT)
    # print(wordsLT)  
    print(trigger_types)

    #             trigger_form.append(wordsLT[i][j-1])
    #             trigger_form.append( wordsLT[i][j].lower() + "trigger")
    #             trigger_form.append(wordsLT[i][j+1])
    #             break

    # print(trigger_form)
                
            
                

read_trigger_config("triggers.txt")